module.exports = grammar({
  name: 'mfc',

  rules: {
    source_file: $ => seq(
      $._firstSector,
      repeat($.sector)
    ),

    _firstSector: $ => seq(
      $.manufBlock,
      $.block,
      $.block,
      $.sectorTrailer
    ),

    sector: $ => seq(
      $.block,
      $.block,
      $.block,
      $.sectorTrailer
    ),

    sectorTrailer: $ => seq(
      $.keyA,
      $.ACB,
      $.keyB
    ),

    keyA: $ => /[0-9A-Fa-f]{12}/,
    ACB: $ => /[0-9A-Fa-f]{8}/,
    keyB: $ => /[0-9A-Fa-f]{12}/,

    manufBlock: $ => /[0-9A-Fa-f]{32}/,
    block: $ => /[0-9A-Fa-f]{32}/,
    //byte: $ => /[0-9A-Fa-f]{2}/,
  }
});
